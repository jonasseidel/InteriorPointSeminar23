\section{Guarantees}
\label{guarantees}

The paper offers the following guarantees for a problem
\begin{align*}
	\min_{x \in \mathcal C} f(x)
\end{align*}
where $f \in C^2$ and $\mathcal C$ is the feasible region. In most cases we will be relaxing these constraints, instead choosing to integrate them into the target as barrier functions. However keeping them implicitly can give us better runtime bounds as will become apparent below.
\begin{de}[gaussian/rademacher width]
	Let $\mathcal D \subseteq \mathbb R^d$ be a cone, $M \in \mathbb R^{l\times d}$ possibly randomly distributed. Now if $g,r \in \mathbb R^l$, $\forall_i \colon g_i \sim \mathcal N(0,1)$ or $\forall_i \colon r_i \sim \mathcal U(\{-1,1\})$, then we call
	\begin{align*}
		\mathcal W_M(\mathcal D) \coloneqq \mathbb E\quad  \sup_{\mathclap{d \in \mathcal D \cap \partial B_1(0)}}\quad | \langle g, Md\rangle| \quad\quad\quad
		\mathcal R_M(\mathcal D) \coloneqq \mathbb E \quad \sup_{\mathclap{d \in \mathcal D \cap \partial B_1(0)}}\quad | \langle r,M d\rangle|
	\end{align*}
	the $M$-gaussian or $M$-rademacher width of $\mathcal D$ respectively. If $M$ is not specified the constant identity matrix and $l=d$ should be assumed.
\end{de}
At its most basic the $I$-gaussian width of a set contained in some subspace of dimension $r$ is $\sqrt{r}$.  Regarding upper bounds on the $I$-rademacher width it is possible to show$^{\ref{estrad}}$ using \cite{montgomery1990distribution} that the rademacher width is bounded by $2\sqrt{r}$. \cite{buldygin2013sub} claims that the necessary distributions of the norm are not yet known for many subgaussian vectors, of which the rademacher variables form one. In any case emprical evidence does suggest that $\sqrt{r}$ suffices for rademacher width as well, though I was unable to confirm that.\par\smallskip
Regarding $W_S$ for some ROS distributed matrix, it can be shown$^{\ref{estsgauss}}$, that the width of an $r$-dimensional subspace is at most $\sqrt{r}$. \par\bigskip 
\noindent These inequalities may help in getting useful bounds in the next theorems without too much efford.
\begin{thm}[strict convex]
	\label{strco}
	If for $x^*\coloneqq \arg\min_{x \in \mathcal C} f(x), f \in C^2$, $\varepsilon, \delta \in (0,1)$
	\begin{align*} 
		\bullet\quad  &0 < \lambda_{min} \coloneqq \inf\limits_{\substack{d\in \mathcal K_{x^*} \mathcal C\\ \parallel d \parallel = 1}}\langle d, \hess f(x^*) d\rangle \leq \lambda_{max} \coloneqq \sup\limits_{\substack{d\in \mathcal K_{x^*} \mathcal C\\ \parallel d \parallel = 1}}\langle d, \hess f(x^*) d\rangle\hspace*{3cm}\\
		\bullet \quad &\parallel x_0 - x^* \parallel_2<\delta \frac{\lambda_{min}}{8L}\\
		\bullet \quad &m \geq \frac{c \cdot \ln(n)^4}{\varepsilon^2} \max_{x \in \mathcal C}\mathcal W\left(\left(\hess f^\frac{1}{2}(x) \mathcal K_{x^*}\mathcal C\right) \cap B_1^2(0)\right)\\
		\bullet \quad &\sup_{x \in \mathcal C} \frac{\parallel \hess f(x) - \hess f(x^*) \parallel_2}{\parallel x - x^* \parallel_2} \eqqcolon L_{x^*}
	\end{align*}
	where $\mathcal K_{x^*}\mathcal C$ denotes the tangential cone at $x^*$ of the domain $\mathcal C$ and $c$ some set undisclosed constant factor.\par\bigskip
	
	Then with probability at least $1- c_1 e^{-c_2\frac{m \varepsilon^2}{\ln(n)^4}}$ the inequality
	\begin{align*}
		\parallel x^{k+1} - x^*\parallel_2 \leq \varepsilon\frac{\lambda_{max}}{\lambda_{min}} \parallel x^k - x^* \parallel_2 + \frac{4L}{\lambda_{min}}\parallel x^k - x^* \parallel_2^2
	\end{align*}
	where $c_1, c_2$ are again undisclosed constants (for more on these see \ref{const} or \cite{pilanci2015randomized},\cite{massart2000constants}).
\end{thm}

\textit{Slight alterations have been made to the theorem to better reflect its proof in \cite{pilanci2017newton} and specialize it to the case of ROS sketch matrices.}\par\medskip

\noindent This result will not be proven here, however there is a proven generalization of it in \ref{generalization}, which stays close to the proof presented in \cite{pilanci2017newton}.\par\smallskip

\noindent The above theorem is for the case of strict convex target functions and has somewhat convoluted preconditions and conclusions. A simpler and more powerful result is available for the case of self-concordant targets. 


\iffalse
\SetKwComment{Comment}{/* }{ */}
\begin{algorithm}
	\label{alg:selfc}
	\caption{Iteration + Line search}\label{alg:two}
	\KwData{$\hat \varepsilon$ accuracy, $(a,b)$ line search param, $S$ sketch, $\hat m$ sketch dimension}
	\While{Gradient magnitude greater than $\hat \varepsilon$}{
		Generate new sketch matrix $S$ of dimension $\hat m$\\
		$\eta^{k+1}\coloneqq  (A^\top \hess f(x^k)^{1/2} S^\top S \hess f(x^k)^{1/2} A)^{-1} \nabla f(x_k)$\hfill/* Iterate accoding to \ref{iteration}.*/\\
		\While{$\frac{f(x^k + \mu\eta^{k+1})- f(x^k)}{\mu \lVert \eta^{k+1}\rVert_2} > a\cdot\nabla f(x^k)^\top \frac{\eta^{k+1}}{\lVert \eta^{k+1} \rVert_2}$ \hfill /*Perform backtracking line search*/}{
			$\mu \coloneqq b \cdot \mu $
		}
		$x^{k+1} \coloneqq x^k + \mu \eta^{k+1};\quad k=k+1$
	}
\end{algorithm}\fi
\begin{algorithm}[H]
	\caption{Iteration + Line search}\label{alg:selfc}
	\begin{algorithmic}
		\Require $\hat \varepsilon$ accuracy, $(a,b)$ line search param, $S$ sketch, $\hat m$ sketch dimension
		\Loop{}
		\State Generate new sketch matrix $S$ of dimension $\hat m$
		\State $\eta^{k+1}\coloneqq  \left(A^\top \hess f(x^k)^{1/2} S^\top S \hess f(x^k)^{1/2} A\right)^{-1} \nabla f(x_k)$ \Comment{Solve for newton-sketch step}
		\State \textbf{Break} if $(\nabla f(x^k) \eta^{k+1})^2/2 < \hat \varepsilon$\Comment{Terminate if accuracy reached}\\
		\While{$\frac{f(x^k + \mu\eta^{k+1})- f(x^k)}{\mu \lVert \eta^{k+1}\rVert_2} > a\cdot\nabla f(x^k)^\top \frac{\eta^{k+1}}{\lVert \eta^{k+1} \rVert_2}$}\Comment{Line Search}
		\State $\mu \coloneqq b \cdot \mu $
		\EndWhile\\
		\State $x^{k+1} \coloneqq x^k + \mu \eta^{k+1};\quad k=k+1$
		\EndLoop
	\end{algorithmic}
\end{algorithm}

\begin{thm}[self-c iter bound]
	If $f$ self-concordance and $m \geq c_3 \frac{d}{\delta^2}$, where $\delta$ is the sketch accuracy in every iteration. When executing algorithm \ref{alg:selfc} with ROS sketches, there exists $v=v(a,b,\delta)$, such that with probability at least $1-c_1 N e^{c_2 m}$ the number of necessary iterations to attain $\varepsilon$ accuracy is bounded by
	\begin{align*}
		\frac{f(x^0) - f(x^*)}{v} + 0.65 \log_2 \left(\frac{1}{16\varepsilon}\right)
	\end{align*}
	where $a,b \in (0,1)$ line search parameters.
\end{thm}

%\textit{Slight alterations have been made to the theorem to specialize it to the case of ROS sketch matrices and our scenario of a target function $f$.}\par\medskip

\noindent This, too, will not be proven here. Like \ref{strco} it draws of theorems derived in \cite{pilanci2015randomized}, but it is also extensive in its own right. It introduces numerous helper lemmas which makes it unfit to be recited here.