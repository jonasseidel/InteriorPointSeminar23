\section{Scrutinizing Constants}
\label{const}
Most of the relevant assertions in \cite{pilanci2015randomized} and \cite{pilanci2017newton} claim a probability of $1 - c_1 e^{c_2 \frac{m \delta^2}{w}}$, where $w$ is a term dependent on some width, and $c_1, c_2 \in \mathbb R$ are some undisclosed constants. These constants are not entirely ascertainable from \cite{pilanci2015randomized} alone and at times the reader is challenged to get the constants from other publications. \par\medskip

Below are the explicit probabilities of bad-sets ($p_{\text{assert\_name}}$) for some selection of  assertions in \cite{pilanci2015randomized} and \cite{pilanci2017newton}. The presented constants are largely implied by the reasoning in their respective proofs.
\def\lemsix{
	\begin{tabular}{|c|}
		\hline Lemma 6$\phantom{}^{\cite{pilanci2015randomized}}$\\
		\hline 
		$p_{l6}(\mathcal Y)\coloneqq \exp\left(-\frac{1}{4096+80\delta}\frac {m\delta^2}{\left(\mathcal R(\mathcal Y)+ \sqrt{2(1+\kappa)\log(nm)}\right)^2} \right)^{\text{see \cite{massart2000constants} corr 13}}$
		\\
		\hline
	\end{tabular}
}
\def\lemseven{
	\begin{tabular}{|c|}
	\hline Lemma 7$\phantom{}^{\cite{pilanci2015randomized}}$\\
	\hline 
	$p_{l7}(\mathcal Y)\coloneqq\frac{1}{(nm)^\kappa} + \exp\left(-\frac{1}{4096} \frac{m \delta^2}{\left(\mathcal R(\mathcal Y)+ \sqrt{2(1+\kappa)\log(nm)}\right)^2}\right)$
	\\
	\hline
\end{tabular}
}

\def\proptwo{
	\begin{tabular}{|c|}
		\hline Proposition 2$\phantom{}^{\cite{pilanci2015randomized}}$\\
		\hline 
		$p_{p2}(\mathcal Y)\coloneqq 4\cdot(p_{l6}(\mathcal Y) + p_{l7}(\mathcal Y))$
		\\
		\hline
	\end{tabular}
}

\def\lemfive{
	\begin{minipage}{7cm}
		\centering
		\begin{tabular}{|c|}
			\hline Lemma 5$^{\ref{lem5gen}}$\\
			\hline 
			$\mathcal A \coloneqq \mathcal X \cup \mathcal Y \cup \frac{\mathcal X + \mathcal Y}{2}$\\
			$p_{l5}(\mathcal X, \mathcal Y)\coloneqq p_{p2}(\mathcal A)$
			\\
			\\
			$8\left(\mathcal R(\mathcal A)+ \sqrt{2(1+\kappa)\log(nm)}\right)\frac{\mathcal W_S(\mathcal A)}{\sqrt{m}} \leq \frac{\delta}{4}$
			\\
			\hline
		\end{tabular}\par\medskip
		\adjustbox{ scale=.75,minipage=1.33\textwidth}{
			\begin{center}
				A useful inequality may be
				\begin{align*}
					\mathcal F\left(\mathcal X \cup \mathcal Y \cup \frac{\mathcal X + \mathcal Y}{2}\right) \leq \frac{3}{2}\left(\mathcal F(\mathcal X) + \mathcal F(\mathcal Y)\right)
				\end{align*}
				for widths $\mathcal F \in \{\mathcal R, \mathcal W_{S}\}$
			\end{center}
		}
	\end{minipage}
}

\def\lemfour{
	\begin{tabular}{|c|}
		\hline Lemma 4$^{\ref{lem4gen}}$\\
		\hline 
		$p_{l4}(\mathcal Y)\coloneqq p_{p2}(\mathcal Y)$
		\\
		\\
		$8\left(\mathcal R(\mathcal Y)+ \sqrt{2(1+\kappa)\log(nm)}\right)\frac{\mathcal W_S(\mathcal Y)}{\sqrt{m}} \leq \frac{\delta}{2}$
		\\
		\hline
	\end{tabular}
}

\def\thmone{
	\begin{tabular}{|c|}
		\hline Theorem 1$\phantom{}^{\cite{pilanci2017newton},\ref{strco}}$\\
		\hline 
		$p_{th1} \coloneqq p_{l5}(\{x_k\}, \hess f(x^*)^{\frac{1}{2}} \mathcal K_{x^*}\mathcal C) + p_{l4}(\hess f(x^*)^{\frac{1}{2}} \mathcal K_{x^*}\mathcal C)$
		\\\\
		Inequalities of Lemma 5 and Lemma 4 need to be satisfied.
		\\
		\hline
	\end{tabular}
}
\begin{adjustwidth}{-2cm}{-2cm}
	\begin{center}
		\adjustbox{scale=.85}{\begin{tikzpicture}
				\node[] (lem6) at (0,0) {\lemsix};
				\node[] at ($(lem6) + (10,0)$) (lem7) {\lemseven};
				\node[] at ($(lem6)!.5!(lem7)+(0,2)$) (prop2) {\proptwo};
				\node[] at ($(prop2)+(-5,2)$) (lem5) {\lemfive};
				\node[] at ($(prop2)+(5,2)$) (lem4) {\lemfour};
				\node[] at ($(lem5)!.5!(lem4)+(0,4)$) (thm1) {\thmone};
				\path[draw] (lem4.west) -- (prop2) -- ($(lem5.east)$);
				\path[draw] (lem6) -- (prop2) -- (lem7);
				\path[draw]  ($(lem4.west) + (0,1)$)-- (thm1) -- ($(lem5.east) + (0,1)$);
		\end{tikzpicture}}
	\end{center}
\end{adjustwidth}


The theorems sketch out an eventual exponential decline in bad-set probability with increasing $m$. But notice that when calculating $p_{th1}$ naively the equation $p_{th1} = c_1 e^{c_2 \frac{m \delta^2}{w}}$ is going to result in $c_1 \geq 16$. Thus the lower bounds on the probability are going to be negative for most reasonably sized instances (certainly for all examples of \cite{pilanci2017newton}). It is true, that Theorem 1 can be derived using only Lemma 5, but the issue would still remain with $c_1 = 8$.\par\medskip
Also note that \cite{pilanci2015randomized} offers sharpening of these probabilities in later chapters. Due to time constraints these are not regarded here, but $c_1$ seems to be substantially larger that $1$ for those bounds as well.\par\medskip
\noindent Of course this is not to take away from the empirically demonstrated efficacy of the method.