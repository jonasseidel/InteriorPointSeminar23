\section{Basics}
\label{basics}
\subsection{Iteration}
\label{iteration}
%TODO: change x^n to x_n
For $f,g \in C^2$ consider the problem $\min_{x \in \mathbb R^d} F(x) = \min_{x \in \mathbb R^d} f(x) + g(Ax)$. The newton method uses then the second order taylor polynomial  in $x^n$ $$q_3(x) = f(x^n) + g(Ax^n)  + \langle \nabla f(x^n) + A^\top \nabla g(x^n), x\rangle + \langle x, \left(\hess f(x^n) + A^\top \hess g(x^n) A\right) x\rangle$$
to approximate our problem locally in the current iterate $x^n$. The method presented by the paper further approximates this polynomial by introducing a random \textit{sketch matrix} $S \in \mathbb R^{m \times n}$ ($\mathbb{E} S^\top S = I$) with
$$q_3^S(x) = f(x^n) + g(Ax^n)  + \langle \nabla f(x^n) + A^\top \nabla g(x^n), x\rangle + \langle x, \left(\hess f(x^n) + A^\top \hess g(x^n)^{\frac{1}{2}}S^\top S\hess g(x^n)^{\frac{1}{2}} A\right) x\rangle$$
Now  instead of choosing the next iterate as the minimum of $q_3$ we use $q_3^S$ as a substitute.\par\bigskip

Our iteration on an unconstrained problem will thus be 
%TODO change x_n to x^n
\begin{align*}
	x^{n+1} = x^n+  \left(\hess f(x^n) + A^\top \hess g(x^n)^{\frac{1}{2}}S^\top S\hess g(x^n)^{\frac{1}{2}} A\right)^{-1} \left(\nabla f(x^n) + A^\top \nabla g(x^n) \right)
\end{align*}

if desired this is of course compatible with the usual augmentations done to newton iteration. The paper employs \textit{Backtracking Line Search} with an \textit{Armijo–Goldstein} termination rule. But one may also imagine imposing a \textit{Trust Region} and further approximating the evaluation in a \textit{Dogleg} fashion. In particular one such augmentation might be necessary when the sketch turns out to not be positive definite. This is the case in particular when we have choosen $m < d$, meaning an exceedingly low rank sketch. \par\medskip

% dogleg rule?
% quasi newton on this?

\subsection{Sketch Generation}
\label{sketchgen}
\def\ros{\textit{Randomized Orthogonal Systems}}
The paper proposes several different ways of generating the \textit{sketch matrices}, the most promising of which are the \ros{} (ROS). The approaches vary slightly in their attained approximation guarantees, but what sets the ROS apart is that they enable much faster multiplication.\par\smallskip
Thus when using these matrices we can calculate higher rank sketches at the same performance level. \par\medskip
The increased speed of evaluation is due to the use of \textit{DFT}/\textit{Hadamard} matrices, thus enabling us to employ an adapted version of the \textit{Cooley-Tukey} algorithm to make evaluation of the hessian much faster. This speedup is of course very welcome as we generally assume this to be the limiting factor in asymptotic runtime.\par\bigskip

In the following we will focus entirely on sketches using ROS.\par\smallskip

\begin{de}[Hadamard Matrix \cite{ailon2006approximate}]
	We call $H \in \mathbb R^{2^r \times 2^r}$ for some $r \in \mathbb N$ the Hadamard Matrix iff for any $0 \leq i,j \leq 2^r-1$ (we index starting from 0)
	\begin{align*}
		H_{ij} = \frac{1}{\sqrt{2^{r}}}(-1)^{\langle \binary(i), \binary(j) \rangle}
	\end{align*}
	where sequences $\binary(i),\binary(j) \in \{0,1\}^{\mathbb N_{0}}$ are the unique binary representations of $i,j$ i.e. $i = \sum_{l = 0}^\infty 2^l \binary(i)_l, j = \sum_{l = 0}^\infty 2^l \binary(j)_l$
\end{de}

\begin{de}[ROS]
	\label{rosdef}
	Let $\diag(d)=D \in \mathbb R^{n\times n}, P \in \mathbb R^{m\times n} $ be random matrices such that
	\begin{alignat*}{20}
		d_{i}	 &\sim \mathcal U(\{-1,1\}),\quad &&\forall 1 \leq i \leq n\\
		P_{i-}=e_{r} \text{ , where } r &\sim \mathcal U(\{1,..,n\}),\quad &&\forall 1 \leq i \leq m
	\end{alignat*}
	where $\mathcal U(A)$ is the discrete uniform distribution on some set $A$. Also let $H$ be the  Hadamard Matrix of adequate size. Notice that this requires $\log_2(n) \in \mathbb N^n$.\par\bigskip
	
	\noindent We call the resulting random matrix $\frac{\sqrt{n}}{\sqrt{m}} P H D$ a ROS matrix.  In the following sketch matrices $S$ may be assumed to be of this type.
\end{de}\bigskip

It should be mentioned, that this is a restricted definition from the one proposed in \cite{ailon2006approximate}. There entries of $P$  were zero with probability $1-q$ and otherwise sampled from some normal distribution $\mathcal N(0, q^{-1})$. Thus the result is a sparse matrix for small $q$. Later \cite{ailon2009fast} found that performance could be further improved by changing $P$ in the above way, making it even more sparse and of predictable structure. This allowed them to further adapt the Cooley-Tukey algorithm to this case of reduced rows.\par
The paper under discussing here (\cite{pilanci2017newton})  makes heavy use of the theory developed in \cite{pilanci2015randomized}, where it is implicitly assumed that $P$ had been reduced in the above way. As such we are only concerning us with the ROS sketches as they defined in \ref{rosdef}.